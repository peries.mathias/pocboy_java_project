# Pocboy an pacman like game

Pacman like game "pocboy" create during java class project by Jeanne-Pierrette Vu and Mathias Peries. 
You need eclipse IDE for Java developer for run the game (no .exe or real .run file). 

## Gameplay
To move around, simply use your numeric keypad.
### Loose condition : 
    - touch a ghost
### Win condition : 
    - Eat all the whites dots of the game map.
    - You can win 8 times before the real victory, after each victory the game difficulty increases. 
### Bonus : 
    - the cyan points are buff, if you eat then, you are invincible and ghosts are eatable.
    - During the game butterflies appear in the middle of the map, if you eat then your score increases.
